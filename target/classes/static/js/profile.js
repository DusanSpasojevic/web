$(document).ready(function() {
    // display info and history
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/api/User/Info",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "",
        success: function(cred) {
            $.getJSON( "http://localhost:8080/api/User/" + cred.username, function( data ) {
                $("#username").text(data.username);
                $("#password").text(data.password);
                $("#firstName").text(data.firstName);
                $("#lastName").text(data.lastName);
                $("#role").text(data.role);
                $("#phoneNumber").text(data.phoneNumber);
                $("#email").text(data.email);
                $("#address").text(data.address);

                var items = [];

                if (cred.role === "CUSTOMER")
                {
                    $.getJSON( "http://localhost:8080/api/Basket/My", function( data ) {
                        data.forEach(function(item, index) {
                            items.push("<tr id=\"" + item.id + "\">" +
                                            "<td>" + item.id + "</td>" +
                                            "<td>" + item.time + "</td>" +
                                            "<td>" + item.status + "</td>" +
                                            (item.status === "Kupljeno" ? 
                                            "<td><button class=\"cancel btn btn-light\" data-basketid=\"" + item.id + "\">Cancel</button></td>"
                                            : "") +
                                        "</tr>");
                        });

                        $("#tablePurchaseView").html(
                            "<table class=\"table\">" +
                            "<thead>" +
                            "<tr>" +
                            "<th>ID</th>" +
                            "<th>Time</th>" +
                            "<th>Status</th>" +
                            "<th></th>" +
                            "</tr>" +
                            "</thead>" +
                            "<tbody>" +
                            items.join("") +
                            "</tbody>" +
                            "</table>"
                        );

                        $(".cancel").click(function() {
                            var btn = this;
                            var id = $(btn).data("basketid");
                            $.ajax({
                                type: "POST",
                                url: "http://localhost:8080/api/Basket/Cancel/" + id,
                                success: function() {
                                    $(btn).hide();
                                    $("#" + id + " td:nth-child(3)").text("Otkazano");
                                }
                            })
                        });
                    });
                }
            });
        }
    })
})