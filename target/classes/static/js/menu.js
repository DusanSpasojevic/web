function loadMenu(role) {
  var adminMenu = [
    {
      url: "/",
      title: "Home"
    },
    {
      url: "/administration.html",
      title: "Administration"
    },
    {
      url: "/api/User/ProcessLogout",
      title: "Logout"
    }
  ];

  var customerMenu = [
    {
      url: "/",
      title: "Home"
    },
    {
      url: "/basket.html",
      title: "Basket"
    },
    {
      url: "/favourite.html",
      title: "Favourite"
    },
    {
      url: "/profile.html",
      title: "Profile"
    },
    {
      url: "/api/User/ProcessLogout",
      title: "Logout"
    }
  ];

  var deliveryManMenu = [
    {
      url: "/",
      title: "Home"
    },
    {
      url: "/delivery.html",
      title: "Delivery"
    },
    {
      url: "/api/User/ProcessLogout",
      title: "Logout"
    }
  ];

  var generalMenu = [
    {
          url: "/",
          title: "Home"
    },
    {
      url: "/login.html",
      title: "Login"
    },
    {
      url: "/register.html",
      title: "Register"
    }
  ]

  var items = [];
  var menuItems;

  if (role === "ADMIN") menuItems = adminMenu;
  else if (role === "DELIVERY_MAN") menuItems = deliveryManMenu;
  else if (role === "CUSTOMER") menuItems = customerMenu;
  else menuItems = generalMenu;

  menuItems.forEach(function (it, index) {
    var item =
    '<li class="nav-item">' +
    '<a class="nav-link" href="' +
    it.url +
    '">' +
    it.title +
    "</a>" +
    "</li>";
  items.push(item);
  });

  $("body").prepend(
    '<nav class="navbar navbar-expand-lg navbar-light bg-light">' +
      '<a class="navbar-brand" href="#">Webshop</a>' +
      '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">' +
      '<span class="navbar-toggler-icon"></span>' +
      "</button>" +
      '<div class="collapse navbar-collapse" id="navbarNav">' +
      '<ul class="navbar-nav">' +
      items.join("") +
      "</ul>" +
      "</div>" +
      "</nav>"
  );
}

$(document).ready(function() {
  $.ajax({
    type: "GET",
    url: "http://localhost:8080/api/User/Info",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    data: "",
    success: function (cred) {
      loadMenu(cred.role);
    }
  });
  
});
