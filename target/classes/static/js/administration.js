function loadUsersList(url) {
    $.getJSON( url, function( data ) {
        var items = [];

        data.forEach(function(user, index) {
            var item = "<option data-username=\"" + user.username + "\">" +
                        user.username +
                        "</option>";
            items.push(item);
        });

        $("#usersList").html(
            items.join("")
        )
    });
}

function loadDeliveryManTable(url) {
    $.getJSON( url, function( data ) {
        var headers = [];
        var items = [];

        headers.push("<th>Username</th>");
        headers.push("<th>Password</th>");
        headers.push("<th>First name</th>");
        headers.push("<th>Last name</th>");
        headers.push("<th>Phone number</th>");
        headers.push("<th>Email</th>");
        headers.push("<th>Address</th>");
        headers.push("<th></th>");
        headers.push("<th></th>");

        data.forEach(function(user, index) {
            var item = "<tr id=\"" + user.username + "\">" +
                            "<td>" + user.username + "</td>" +
                            "<td>" + user.password + "</td>" +
                            "<td>" + user.firstName + "</td>" +
                            "<td>" + user.lastName + "</td>" +
                            "<td>" + user.phoneNumber + "</td>" +
                            "<td>" + user.email + "</td>" +
                            "<td>" + user.address + "</td>";
                            
            item += "<td><button class=\"update btn btn-light\" data-username=\"" + user.username + "\" data-toggle=\"modal\" data-target=\"#userModal\">Update" + "</button></td>" +
                    "<td><button class=\"remove btn btn-light\" data-username=\"" + user.username + "\">Remove" + "</button></td>" +
                    "</tr>";

            items.push(item);
        })


        var articleButton = "<button id=\"addDeliveryMan\" class=\"btn btn-primary float-right\"" +
            "data-toggle=\"modal\" data-target=\"#userModal\"" +
            ">" +
            "Add delivery man" +
            "</button>";
        
        $("#tableDeliveryManView").html(
            "<table class=\"table\">" +
            "<thead>" +
            headers.join("") +
            "</thead>" +
            "<tbody>" +
            items.join("") +
            "</tbody>" +
            "</table>" + articleButton
        )

        $(".update").click(function() {
            var id = $(this).data("username");
            $("#username").attr('readonly', true);
            $("#username").val($("#" + id + " td:nth-child(1)").text());
            $("#password").val($("#" + id + " td:nth-child(2)").text());
            $("#firstName").val($("#" + id + " td:nth-child(3)").text());
            $("#lastName").val($("#" + id + " td:nth-child(4)").text());
            $("#email").val($("#" + id + " td:nth-child(5)").text());
            $("#address").val($("#" + id + " td:nth-child(6)").text());
    
            $("#magicButton").text("Apply");
        })

        $(".remove").click(function() {
            var username = $(this).data("username");
            
            $.ajax({
                type: "DELETE",
                url: "http://localhost:8080/api/DeliveryMan/" + username,
                success: function(data) {
                    loadDeliveryManTable(url);
                },
                error: function(data) {
                    alert(data.responseJSON.error);
                }
            })
        })

        $("#addDeliveryMan").click(function() {
            $("#username").attr('readonly', false);
            $("#username").val("");
            $("#password").val("");
            $("#firstName").val("");
            $("#lastName").val("");
            $("#email").val("");
            $("#address").val("");
    
            $("#magicButton").text("Add");
        });
    });
}

function loadReport(data) {
    $("#tableReportView").html(
       "<table class=\"table\">" +
           "<thead>" +
               "<tr>" +
                   "<th witch=\"50%\"></th>" +
                   "<th width=\"50%\"></th>" +
               "</tr>" +
           "</thead>" +
           "<tbody>" +
               "<tr>" +
                   "<td>" +
                   "Canceled" +
                   "</td>" +
                   "<td>" +
                   data.canceled +
                   "</td>" +
               "</tr>" +
               "<tr>" +
                                  "<td>" +
                                  "Delivered" +
                                  "</td>" +
                                  "<td>" +
                                  data.delivered +
                                  "</td>" +
                              "</tr>" +
                              "<tr>" +
                                                 "<td>" +
                                                 "Sum" +
                                                 "</td>" +
                                                 "<td>" +
                                                 data.sum +
                                                 "</td>" +
                                             "</tr>" +
                   "</tbody>" +
                   "</table>"
    );
}


$(document).ready(function() {
    loadUsersList("http://localhost:8080/api/User/all");
    loadDeliveryManTable("http://localhost:8080/api/DeliveryMan/all");

    $("#changeRole").submit(function(event) {
        event.preventDefault();


        $.ajax({
            type: "POST",
            url: "http://localhost:8080/api/User/ChangeRole",
            data: {
                username: $("#usersList").val(),
                newRole: $("#newRole").val()
            },
            success: function(data) {
                loadDeliveryManTable("http://localhost:8080/api/DeliveryMan/all");
            },
            error: function(data) {
                alert("Something went wrong.");
            }
        });
    })

    $("#magicButton").click(function() {
        data = JSON.stringify({
            username: $("#username").val(),
            password: $("#password").val(),
            firstName: $("#firstName").val(),
            lastName: $("#lastName").val(),
            phoneNumber: $("#phoneNumber").val(),
            email: $("#email").val(),
            address: $("#address").val()
        })

        $.ajax({
            type: "POST",
            url: "http://localhost:8080/api/DeliveryMan/Add",
            dataType: "json",
            contentType: "application/json",
            data: data,
            success: function (data) {
                $('.modal').modal('hide');
                loadUsersList("http://localhost:8080/api/User/all");
                loadDeliveryManTable("http://localhost:8080/api/DeliveryMan/all");
            },
            error: function(data) {
            }
        })
    })

    $("#daily").click(function() {
        $.getJSON( "http://localhost:8080/api/Basket/Report/Daily", function( data ) {
            loadReport(data);
        });
    })

    $("#weekly").click(function() {
        $.getJSON( "http://localhost:8080/api/Basket/Report/Weekly", function( data ) {
            loadReport(data);
        });
    })

    $("#monthly").click(function() {
        $.getJSON( "http://localhost:8080/api/Basket/Report/Monthly", function( data ) {
            loadReport(data);
        });
    })
});