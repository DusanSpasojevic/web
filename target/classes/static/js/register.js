$(document).ready(function() {
  $("#registration").submit(function(event) {
    event.preventDefault();

    var newCustomerJSON = JSON.stringify({
      username: $("#username").val(),
      password: $("#password").val(),
      firstName: $("#firstName").val(),
      lastName: $("#lastName").val(),
      phoneNumber: $("#phoneNumber").val(),
      email: $("#email").val(),
      address: $("#address").val()
    });

    $.ajax({
      type: "POST",
      url: "http://localhost:8080/api/Customer/Register",
      dataType: "json",
      contentType: "application/json",
      data: newCustomerJSON,
      success: function(data) {
        window.location.replace("http://localhost:8080/login.html");
      },
      error: function(data) {
        $("#message").text(data.error);
      }
    });
  });
});
