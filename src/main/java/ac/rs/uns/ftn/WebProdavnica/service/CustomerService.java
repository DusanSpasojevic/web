package ac.rs.uns.ftn.WebProdavnica.service;

import ac.rs.uns.ftn.WebProdavnica.model.Article;
import ac.rs.uns.ftn.WebProdavnica.model.BasketItem;
import ac.rs.uns.ftn.WebProdavnica.model.Customer;
import ac.rs.uns.ftn.WebProdavnica.repository.ArticleRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.KorpaItemRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private KorpaItemRepository korpaItemRepository;

    public Customer create(Customer customer) throws Exception {
        if (customerRepository.existsById(customer.getUsername()))
            throw new Exception("Username already exists.");

        Customer newCustomer = this.customerRepository.save(customer);
        return newCustomer;
    }

    public void addFavouriteArticle(String korisnickoIme, Long articleId) throws Exception {
        Customer customer = this.customerRepository.getOne(korisnickoIme);

        if (customer == null)
            throw new Exception("Username is not valid for this request.");

        Article article = this.articleRepository.getOne(articleId);

        if (article == null)
            throw new Exception("Article id is not valid.");

        if (customer.getFavouriteArticles().contains(article))
            throw new Exception("Item is already favourite.");

        customer.addFavouriteArticle(article);
        this.customerRepository.save(customer);
    }

    public Set<Article> getFavouriteArticles(String korisnickoIme) throws Exception {
        Customer customer = this.customerRepository.getOne(korisnickoIme);

        if (customer == null)
            throw new Exception("Username is not valid for this request.");

        return customer.getFavouriteArticles();
    }

    public Customer loadKupac(String username) {
        return this.customerRepository.getOne(username);
    }

    public void buyArticle(String username, Long articleId, Integer quantity) throws Exception {
        Customer customer = this.customerRepository.getOne(username);

        if (customer == null)
            throw new Exception("Username is not valid.");

        Article article = this.articleRepository.getOne(articleId);

        if (article == null)
            throw new Exception("Article id is not valid.");

        if (article.getQuantity() < quantity || quantity <= 0)
            throw new Exception("Quantity is not valid.");

        BasketItem item = new BasketItem(article, quantity);

        article.setQuantity(article.getQuantity() - item.getQuantity());

        articleRepository.save(article);

        this.korpaItemRepository.save(item);
        customer.addArticleToBasket(item);
        this.customerRepository.save(customer);
    }

    public Set<BasketItem> getBasket(String username) {
        Customer customer = this.customerRepository.getOne(username);

        return customer.getBasket();
    }
}
