package ac.rs.uns.ftn.WebProdavnica.controller;

import ac.rs.uns.ftn.WebProdavnica.model.Article;
import ac.rs.uns.ftn.WebProdavnica.model.Customer;
import ac.rs.uns.ftn.WebProdavnica.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/Customer")
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping("/Register")
    public ResponseEntity<?> addCustomer(@Valid @RequestBody Customer customer, HttpSession session) {
        try {
            if (session.getAttribute("username") != null)
                throw new Exception("You must logout to perform this action.");

            customer.setRole("CUSTOMER");
            Customer newCustomer = customerService.create(customer);

            return new ResponseEntity<Customer>(newCustomer, HttpStatus.CREATED);
        } catch (Exception e) {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("error", e.getMessage());
            return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/AddFavourite/{articleId}")
    public ResponseEntity<?> addFavouriteArticle(@PathVariable Long articleId, HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("CUSTOMER"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        try {
            customerService.addFavouriteArticle(username, articleId);
            status.put("status", "Added new favourite article.");

            return new ResponseEntity<>(status, HttpStatus.OK);
        } catch (Exception e) {
            status.put("error", e.getMessage());
        }

        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/Favourite")
    public ResponseEntity<?> getFavouriteArticle(HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("CUSTOMER"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        try {
            return new ResponseEntity<Set<Article>>(this.customerService.getFavouriteArticles(username), HttpStatus.OK);
        } catch (Exception e) {
        }

        return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);
    }

    @PostMapping("/AddToBasket")
    public ResponseEntity<?> buyArticle(@RequestParam Long articleId, @RequestParam Integer quantity,
                                        HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("CUSTOMER"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        try {
            this.customerService.buyArticle(username, articleId, quantity);
            status.put("status", "ok");
            return new ResponseEntity<>(status, HttpStatus.OK);
        } catch (Exception e) {
            status.put("error", e.getMessage());
        }

        return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);
    }

    @GetMapping("/BasketArticles")
    public ResponseEntity<?> getBasketArticles(HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("CUSTOMER"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        //TODO handle invalid username...
        return new ResponseEntity<>(this.customerService.getBasket(username), HttpStatus.OK);
    }
}
