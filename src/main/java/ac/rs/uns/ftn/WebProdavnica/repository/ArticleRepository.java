package ac.rs.uns.ftn.WebProdavnica.repository;

import ac.rs.uns.ftn.WebProdavnica.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    List<Article> findArticleByCategory(String category);

    Set<Article> findByNameIgnoreCaseContaining(String name);

    Set<Article> findByDescriptionIgnoreCaseContaining(String description);

    Set<Article> findByPrice(Double price);

    @Query(value = "SELECT DISTINCT a.category FROM Article a")
    Set<String> findAllCategories();
}
