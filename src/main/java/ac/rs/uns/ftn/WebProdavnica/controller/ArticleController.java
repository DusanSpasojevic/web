package ac.rs.uns.ftn.WebProdavnica.controller;

import ac.rs.uns.ftn.WebProdavnica.model.Article;
import ac.rs.uns.ftn.WebProdavnica.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/Article")
@CrossOrigin
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    //TODO separate update...
    @PostMapping("/Add")
    public ResponseEntity<?> addArticle(@Valid @RequestBody Article article, HttpSession session) {
        String role = (String) session.getAttribute("role");

        Map<String, String> res = new HashMap<>();

        if (role != null && !role.equals("ADMIN"))
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);

        try {
            Article newArticle = articleService.create(article);

            return new ResponseEntity<Article>(newArticle, HttpStatus.CREATED);
        } catch (Exception e) {
            res.put("error", e.getMessage());

            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllArticles() {
        return new ResponseEntity<>(this.articleService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/category/{category}")
    public ResponseEntity<?> getAllArticles(@PathVariable(name = "category") String category) {
        return new ResponseEntity<>(this.articleService.findAllByCategory(category), HttpStatus.OK);
    }

    @GetMapping("/Search")
    public ResponseEntity<?> searchArticle(@RequestParam(required = false) String name,
                                          @RequestParam(required = false) String description,
                                          @RequestParam(required = false) Double price) {
        Map<String, String> res = new HashMap<>();

        if (name == null && description == null && price == null) {
            res.put("error", "You must pass one parameter, at least.");
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(this.articleService.search(name, description, price), HttpStatus.OK);
    }

    @DeleteMapping("/{articleId}")
    public ResponseEntity<?> deleteArticle(@PathVariable Long articleId, HttpSession session) {
        String role = (String) session.getAttribute("role");

        Map<String, String> res = new HashMap<>();

        if (role != null && !role.equals("ADMIN"))
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);

        try {
            this.articleService.delete(articleId);
            res.put("status", "ok");
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            res.put("error", e.getMessage());
        }


        return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/Categories")
    public ResponseEntity<?> getAllCategories() {
        return new ResponseEntity<>(this.articleService.getCategories(), HttpStatus.OK);
    }
}