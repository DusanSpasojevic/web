package ac.rs.uns.ftn.WebProdavnica.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class DeliveryMan extends User {
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Basket> assigned;

    public DeliveryMan() {
    }

    public DeliveryMan(User user) {
        super(user);
    }

    public void removeAssigned(Basket basket) {
        assigned.remove(basket);
    }

    public Set<Basket> getAssigned() {
        return assigned;
    }

    public void assignBasket(Basket basket) {
        basket.setDeliveryMan(this);
        assigned.add(basket);
    }
}
