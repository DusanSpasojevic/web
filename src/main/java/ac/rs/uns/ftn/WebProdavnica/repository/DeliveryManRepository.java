package ac.rs.uns.ftn.WebProdavnica.repository;

import ac.rs.uns.ftn.WebProdavnica.model.DeliveryMan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryManRepository extends JpaRepository<DeliveryMan, String> {
}
