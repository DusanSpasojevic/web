package ac.rs.uns.ftn.WebProdavnica.service;

import ac.rs.uns.ftn.WebProdavnica.model.Customer;
import ac.rs.uns.ftn.WebProdavnica.model.DeliveryMan;
import ac.rs.uns.ftn.WebProdavnica.model.User;
import ac.rs.uns.ftn.WebProdavnica.repository.CustomerRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.DeliveryManRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DeliveryManRepository deliveryManRepository;

    public User loadKorisnik(String korisnickoIme, String lozinka) throws Exception {
        User user = this.userRepository.getOne(korisnickoIme);

        if (user == null)
            throw new Exception("Korisnicko ime nije validno.");

        if (lozinka != null && !user.getPassword().equals(lozinka))
            throw new Exception("Lozinka nije validna.");

        return user;
    }

    //TODO something went terrible wrong
    public void changeRole(String username, String newRole) throws Exception {
        User user = this.userRepository.getOne(username);

        if (user == null)
            throw new Exception("Username is not valid.");


        if (newRole.equals(user.getRole()))
            return;

        this.userRepository.deleteById(user.getUsername());
        user.setRole(newRole);

        if (newRole.equals("ADMIN")) {
            User x = new User(user);
            System.out.println(x);
            this.userRepository.save(x);
        }
        else if (newRole.equals("CUSTOMER"))
            this.customerRepository.save(new Customer(user));
        else if (newRole.equals("DELIVERY_MAN"))
            this.deliveryManRepository.save(new DeliveryMan(user));
/*
        if (newRole.equals(user.getRole()))
            return;
        else if (newRole.equals("ADMIN")) {
            user.setRole(newRole);
            User x = new User(user);
            this.userRepository.deleteById(user.getUsername());
            System.out.println("Dest: " + x);
            //this.userRepository.save(x);
        } else if (newRole.equals("CUSTOMER")) {
            user.setRole(newRole);
            Customer x = new Customer(user);
            System.out.println(x);
            this.userRepository.deleteById(user.getUsername());
            //this.customerRepository.save(new Customer(user));
        } else if (newRole.equals("DELIVERY_MAN")) {
            user.setRole(newRole);
            DeliveryMan x = new DeliveryMan(user);
            System.out.println("Dest: " + x);
            //this.deliveryManRepository.deleteById(user.getUsername());
            //this.deliveryManRepository.save(new DeliveryMan(user));
        }
 */
    }

    public List<User> getAll() {
        return this.userRepository.findAll();
    }
}
