package ac.rs.uns.ftn.WebProdavnica.repository;

import ac.rs.uns.ftn.WebProdavnica.model.BasketItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KorpaItemRepository extends JpaRepository<BasketItem, String> {
}
