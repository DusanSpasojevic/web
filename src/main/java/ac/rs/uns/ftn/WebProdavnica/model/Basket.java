package ac.rs.uns.ftn.WebProdavnica.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.sql.Date;

@Entity
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<BasketItem> articles;

    @Column
    private Date time;

    @OneToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Customer customer;

    @OneToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private DeliveryMan deliveryMan;

    @Column
    private String status;

    public Basket() {
    }

    public Basket(Customer customer) {
        this.customer = customer;

        this.articles = new HashSet<>();
        Iterator<BasketItem> it = customer.getBasket().iterator();
        while (it.hasNext())
            this.articles.add(it.next());

        LocalDate today = LocalDate.now(ZoneId.of("Europe/Belgrade"));

        //TODO remove, just testing
        //Random r = new Random();
        //today = today.minusDays(r.nextInt(35));

        this.time = Date.valueOf(today);
        this.status = Constants.BOUGHT;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<BasketItem> getArticles() {
        return articles;
    }

    public void setArticles(Set<BasketItem> articles) {
        this.articles = articles;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public DeliveryMan getDeliveryMan() {
        return deliveryMan;
    }

    public void setDeliveryMan(DeliveryMan deliveryMan) {
        this.deliveryMan = deliveryMan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
