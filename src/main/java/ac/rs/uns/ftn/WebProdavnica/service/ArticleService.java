package ac.rs.uns.ftn.WebProdavnica.service;

import ac.rs.uns.ftn.WebProdavnica.model.Article;
import ac.rs.uns.ftn.WebProdavnica.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Service
public class ArticleService {
    @Autowired
    private ArticleRepository articleRepository;

    // create or update
    public Article create(Article article) throws Exception {
        Article newArticle = this.articleRepository.save(article);
        return newArticle;
    }

    public List<Article> findAllByCategory(String category) {
        return this.articleRepository.findArticleByCategory(category);
    }

    //TODO maybe rename to: find by multiple or something...
    public List<Article> search(String name, String description, Double price) {
        ArrayList<Set<Article>> resultSets = new ArrayList<Set<Article>>();

        if (name != null)
            resultSets.add(
                    this.articleRepository.findByNameIgnoreCaseContaining(name));

        if (description != null)
            resultSets.add(
                    this.articleRepository.findByDescriptionIgnoreCaseContaining(description));

        if (price != null)
            resultSets.add(
                    this.articleRepository.findByPrice(price));

        Iterator<Set<Article>> it = resultSets.iterator();
        Set<Article> result = it.next();

        while (it.hasNext())
            result.retainAll(it.next());

        return new ArrayList<>(result);
    }

    public List<Article> findAll() {
        return this.articleRepository.findAll();
    }

    public void delete(Long id) throws Exception {
        if (!this.articleRepository.existsById(id))
            throw new Exception("Article id is not valid.");

        this.articleRepository.deleteById(id);
    }

    public Set<String> getCategories() {
        return this.articleRepository.findAllCategories();
    }
}
