package ac.rs.uns.ftn.WebProdavnica.service;

import ac.rs.uns.ftn.WebProdavnica.model.*;
import ac.rs.uns.ftn.WebProdavnica.repository.ArticleRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.BasketRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.CustomerRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.DeliveryManRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class BasketService {
    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private DeliveryManRepository deliveryManRepository;

    public Basket create(String username) throws Exception {
        Customer customer = this.customerRepository.getOne(username);

        if (customer.getBasket().isEmpty())
            throw new Exception("Basket is empty.");

        Basket basket = new Basket(customer);

        customer.clearBasket();

        this.customerRepository.save(customer);
        Basket newBasket = this.basketRepository.save(basket);

        return newBasket;
    }

    public Set<Basket> getUserBaskets(String username) {
        Customer customer = this.customerRepository.getOne(username);
        return this.basketRepository.findByCustomer(customer);
    }

    public Set<Basket> getBought() {
        return this.basketRepository.findByStatus(Constants.BOUGHT);
    }

    public void cancel(String username, Long basketId) {
        Basket basket = this.basketRepository.getOne(basketId);

        if (!basket.getCustomer().getUsername().equals(username))
            return;

        // return items
        for (BasketItem item : basket.getArticles()) {
            Article a = item.getArticle();
            a.setQuantity(a.getQuantity() + item.getQuantity());
            this.articleRepository.save(a);
        }

        basket.setStatus(Constants.CANCELED);
        this.basketRepository.save(basket);
    }

    public void setBasketDelivering(String username, Long basketId) throws Exception {
        Basket basket = this.basketRepository.getOne(basketId);
        DeliveryMan man = this.deliveryManRepository.getOne(username);

        //TODO check...

        for (Basket b : man.getAssigned()) {
            if (b.getStatus().equals(Constants.DELIVERING))
                throw new Exception("You are already delivering...");
        }

        basket.setStatus(Constants.DELIVERING);
        basket.setDeliveryMan(man);
        man.assignBasket(basket);

        this.basketRepository.save(basket);
        this.deliveryManRepository.save(man);
    }

    public Map<String, String> getDeliveringForDeliveryMan(String username) {
        DeliveryMan deliveryMan = this.deliveryManRepository.getOne(username);
        Map<String, String> map = new HashMap<>();

        for (Basket b : deliveryMan.getAssigned()) {
            if (b.getStatus().equals(Constants.DELIVERING)) {
                map.put("id", b.getId().toString());
                map.put("time", b.getTime().toString());
                break;
            }
        }

        return map;
    }

    public void deliver(String username, Long basketId) {
        Basket basket = this.basketRepository.getOne(basketId);
        DeliveryMan man = this.deliveryManRepository.getOne(username);

        //TODO check

        basket.setStatus(Constants.DELIVERED);
        this.basketRepository.save(basket);
    }

    public Map<String, Number> getReport(ReportConstants period) {
        LocalDate today = LocalDate.now(ZoneId.of("Europe/Belgrade"));

        LocalDate target = null;

        if (period == ReportConstants.DAILY)
            target = today.minusDays(1);
        else if (period == ReportConstants.WEEKLY)
            target =  today.minusWeeks(1);
        else if (period == ReportConstants.MONTHLY)
            target = today.minusMonths(1);

        Set<Basket> allBaskets = this.basketRepository.findAfterDate(Date.valueOf(target));

        int totalDelivered = 0;
        int totalCanceled = 0;
        double sum = 0.0;

        for (Basket b : allBaskets) {
            if (b.getStatus().equals(Constants.DELIVERED)) {
                totalDelivered++;
                for (BasketItem basketItem : b.getArticles())
                    sum += basketItem.getQuantity() * basketItem.getArticle().getPrice();
            } else if (b.getStatus().equals(Constants.CANCELED))
                totalCanceled++;
        }

        Map<String, Number> report = new HashMap<>();

        report.put("delivered", totalDelivered);
        report.put("canceled", totalCanceled);
        report.put("sum", sum);

        return report;
    }


}
