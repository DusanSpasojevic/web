package ac.rs.uns.ftn.WebProdavnica;

import ac.rs.uns.ftn.WebProdavnica.model.Article;
import ac.rs.uns.ftn.WebProdavnica.model.Customer;
import ac.rs.uns.ftn.WebProdavnica.model.DeliveryMan;
import ac.rs.uns.ftn.WebProdavnica.model.User;
import ac.rs.uns.ftn.WebProdavnica.repository.ArticleRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.CustomerRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.DeliveryManRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.UserRepository;
import ac.rs.uns.ftn.WebProdavnica.service.DeliveryManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebProdavnicaApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private DeliveryManRepository deliveryManRepository;

	@Override
	public void run(String... args) {
		// Init administration account.
		User admin = new User();
		admin.setUsername("admin");
		admin.setPassword("admin");
		admin.setRole("ADMIN");

		this.userRepository.save(admin);

		// Customer
		Customer customer = new Customer();
		customer.setRole("CUSTOMER");
		customer.setUsername("cus");
		customer.setPassword("cus");

		this.customerRepository.save(customer);

		// Init articles.
		Article a = new Article();
		a.setName("Plazma keks");
		a.setCategory("Keksovi");
		a.setDescription("...opis plazme...");
		a.setPrice(10.20);
		a.setQuantity(10);

		Article b = new Article();
		b.setName("Jafa keks");
		b.setCategory("Keksovi");
		b.setDescription("...opis jafe...");
		b.setPrice(19.20);
		b.setQuantity(15);

		Article c = new Article();
		c.setName("Mars");
		c.setCategory("Cokolade");
		c.setDescription("...najbolja cokoladica...");
		c.setPrice(50.82);
		c.setQuantity(400);

		this.articleRepository.save(a);
		this.articleRepository.save(b);
		this.articleRepository.save(c);

		DeliveryMan d = new DeliveryMan();
		d.setRole("DELIVERY_MAN");
		d.setUsername("nikola");
		d.setPassword("nikolic");

		this.deliveryManRepository.save(d);
	}

	public static void main(String[] args) {
		SpringApplication.run(WebProdavnicaApplication.class, args);
	}

}
