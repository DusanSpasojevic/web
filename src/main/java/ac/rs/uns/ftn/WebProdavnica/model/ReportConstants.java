package ac.rs.uns.ftn.WebProdavnica.model;

public enum ReportConstants {
    DAILY,
    WEEKLY,
    MONTHLY
}
