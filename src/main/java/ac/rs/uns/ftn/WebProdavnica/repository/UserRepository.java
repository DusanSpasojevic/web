package ac.rs.uns.ftn.WebProdavnica.repository;

import ac.rs.uns.ftn.WebProdavnica.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
