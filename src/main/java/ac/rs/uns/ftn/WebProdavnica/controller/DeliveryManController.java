package ac.rs.uns.ftn.WebProdavnica.controller;

import ac.rs.uns.ftn.WebProdavnica.model.DeliveryMan;
import ac.rs.uns.ftn.WebProdavnica.repository.DeliveryManRepository;
import ac.rs.uns.ftn.WebProdavnica.service.DeliveryManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/DeliveryMan")
@CrossOrigin
public class DeliveryManController {
    @Autowired
    DeliveryManService deliveryManService;

    @PostMapping("/Add")
    public ResponseEntity<?> addDeliveryMan(@Valid @RequestBody DeliveryMan deliveryMan, HttpSession session) {
        String role = (String) session.getAttribute("role");

        if (role != null && !role.equals("ADMIN"))
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);

        deliveryMan.setRole("DELIVERY_MAN");
        DeliveryMan newDeliveryMan = this.deliveryManService.create(deliveryMan);

        return new ResponseEntity<>(newDeliveryMan, HttpStatus.CREATED);
    }

    @DeleteMapping("/{username}")
    public ResponseEntity<?> deleteDeliveryMan(@PathVariable String username, HttpSession session) {
        String role = (String) session.getAttribute("role");

        Map<String, String> res = new HashMap<>();

        if (role != null && !role.equals("ADMIN"))
           return new ResponseEntity<>("", HttpStatus.FORBIDDEN);

        try {
            this.deliveryManService.delete(username);
            res.put("status", "ok");
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            res.put("error", e.getMessage());
        }

        return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll(HttpSession session) {
        String role = (String) session.getAttribute("role");

        if (role != null && role.equals("ADMIN"))
            return new ResponseEntity<>(this.deliveryManService.getAll(), HttpStatus.OK);

        return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
    }
}
