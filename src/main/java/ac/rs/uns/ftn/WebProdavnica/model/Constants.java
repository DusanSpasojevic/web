package ac.rs.uns.ftn.WebProdavnica.model;

public class Constants {
    public static String BOUGHT = "Kupljeno";
    public static String DELIVERING = "Dostava u toku";
    public static String CANCELED = "Otkazano";
    public static String DELIVERED = "Dostavjeno";
}
