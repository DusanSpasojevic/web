package ac.rs.uns.ftn.WebProdavnica.repository;

import ac.rs.uns.ftn.WebProdavnica.model.Basket;
import ac.rs.uns.ftn.WebProdavnica.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.Set;

public interface BasketRepository extends JpaRepository<Basket, Long> {
    Set<Basket> findByCustomer(Customer customer);
    Set<Basket> findByStatus(String status);

    @Query("SELECT b FROM Basket b WHERE time >= :begin")
    Set<Basket> findAfterDate(@Param("begin")Date begin);
}
