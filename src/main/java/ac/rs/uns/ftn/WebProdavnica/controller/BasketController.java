package ac.rs.uns.ftn.WebProdavnica.controller;

import ac.rs.uns.ftn.WebProdavnica.model.ReportConstants;
import ac.rs.uns.ftn.WebProdavnica.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/Basket")
@CrossOrigin
public class BasketController {
    @Autowired
    private BasketService basketService;

    @PostMapping("/Add")
    public ResponseEntity<?> addBucket(HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("CUSTOMER"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        try {
            return new ResponseEntity<>(this.basketService.create(username), HttpStatus.CREATED);
        } catch (Exception e) {
            status.put("error", e.getMessage());
        }

        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/My")
    public ResponseEntity<?> getUserBuckets(HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("CUSTOMER"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        //TODO check empty list...
        return new ResponseEntity<>(this.basketService.getUserBaskets(username), HttpStatus.OK);
    }

    @GetMapping("/Bought")
    public ResponseEntity<?> getBoughtBaskets(HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("DELIVERY_MAN"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        return new ResponseEntity<>(this.basketService.getBought(), HttpStatus.OK);
    }

    @PostMapping("/Delivering/{basketId}")
    public ResponseEntity<?> setBasketDelivering(@PathVariable Long basketId, HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("DELIVERY_MAN"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        try {
            this.basketService.setBasketDelivering(username, basketId);
            status.put("status", "ok");
            return new ResponseEntity<>(status, HttpStatus.OK);
        } catch (Exception e) {
            status.put("error", e.getMessage());
        }

        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/Deliver/{basketId}")
    public ResponseEntity<?> deliverBasket(@PathVariable Long basketId, HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("DELIVERY_MAN"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        try {
            this.basketService.deliver(username, basketId);
            status.put("status", "ok");
            return new ResponseEntity<>(status, HttpStatus.OK);
        } catch (Exception e) {
            status.put("error", e.getMessage());
        }

        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/Delivering")
    public ResponseEntity<?> getDelivering(HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        Map<String, String> status = new HashMap<>();

        if (role != null && !role.equals("DELIVERY_MAN"))
            return new ResponseEntity<>(status, HttpStatus.FORBIDDEN);

        return new ResponseEntity<>(this.basketService.getDeliveringForDeliveryMan(username), HttpStatus.OK);
    }

    @PostMapping("/Cancel/{baskedId}")
    public ResponseEntity<?> cancelBasket(@PathVariable Long baskedId, HttpSession session) {
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        if (role != null && !role.equals("CUSTOMER"))
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);

        this.basketService.cancel(username, baskedId);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @GetMapping("/Report/Daily")
    public ResponseEntity<?> getDailyReport(HttpSession session) {
        String role = (String) session.getAttribute("role");

        if (role != null && role.equals("ADMIN"))
            return new ResponseEntity<>(this.basketService.getReport(ReportConstants.DAILY), HttpStatus.OK);

        return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
    }

    @GetMapping("/Report/Weekly")
    public ResponseEntity<?> getWeeklyReport(HttpSession session) {
        String role = (String) session.getAttribute("role");

        if (role != null && role.equals("ADMIN"))
            return new ResponseEntity<>(this.basketService.getReport(ReportConstants.WEEKLY), HttpStatus.OK);

        return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
    }

    @GetMapping("/Report/Monthly")
    public ResponseEntity<?> getYearlyReport(HttpSession session) {
        String role = (String) session.getAttribute("role");

        if (role != null && role.equals("ADMIN"))
            return new ResponseEntity<>(this.basketService.getReport(ReportConstants.MONTHLY), HttpStatus.OK);

        return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
    }
}
