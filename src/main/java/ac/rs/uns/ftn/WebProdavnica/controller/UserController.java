package ac.rs.uns.ftn.WebProdavnica.controller;

import ac.rs.uns.ftn.WebProdavnica.model.User;
import ac.rs.uns.ftn.WebProdavnica.service.UserService;
import org.omg.CORBA.RepositoryIdHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/User")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/Info")
    public ResponseEntity<?> getInfo(HttpSession session) {
        Map<String, String> res = new HashMap<>();

        res.put("username", (String) session.getAttribute("username"));
        res.put("role", (String) session.getAttribute("role"));

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping("/ProcessLogin")
    public ResponseEntity<?> loginUser(@RequestParam(required = true) String username,
                                          @RequestParam(required = true) String password, HttpSession session) {
        Map<String, String> res = new HashMap<>();

        if (session.getAttribute("username") != null)
        {
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
        }

        try {
            User user = userService.loadKorisnik(username, password);

            res.put("status", "ok");
            session.setAttribute("username", username);
            session.setAttribute("role", user.getRole());
        } catch (Exception e) {
            res.put("error", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }


    @GetMapping("/ProcessLogout")
    public RedirectView logoutUser(HttpSession session) {
        if (session.getAttribute("username") == null)
        {
            return new RedirectView("/index.html");
        }

        session.invalidate();

        return new RedirectView("/index.html");
    }

    //TODO find better way to check role!
    @PostMapping("/ChangeRole")
    public ResponseEntity<?> updateUser(@RequestParam String username, @RequestParam String newRole) {
        Map<String, String> res = new HashMap<>();

        try {
            this.userService.changeRole(username, newRole);
            res.put("status", "ok");
        } catch (Exception e) {
            res.put("status", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<?> getUser(@PathVariable String username) throws Exception {
        User user = this.userService.loadKorisnik(username, null);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(this.userService.getAll(), HttpStatus.OK);
    }
}
