package ac.rs.uns.ftn.WebProdavnica.service;

import ac.rs.uns.ftn.WebProdavnica.model.Basket;
import ac.rs.uns.ftn.WebProdavnica.model.Constants;
import ac.rs.uns.ftn.WebProdavnica.model.DeliveryMan;
import ac.rs.uns.ftn.WebProdavnica.repository.BasketRepository;
import ac.rs.uns.ftn.WebProdavnica.repository.DeliveryManRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class DeliveryManService {
    @Autowired
    private DeliveryManRepository deliveryManRepository;

    @Autowired
    private BasketRepository basketRepository;

    public DeliveryMan create(DeliveryMan deliveryMan) {
        return this.deliveryManRepository.save(deliveryMan);
    }

    public void delete(String username) throws Exception {
        DeliveryMan deliveryMan = this.deliveryManRepository.getOne(username);

        if (deliveryMan == null)
            throw new Exception("Invalid username.");

        this.deliveryManRepository.delete(deliveryMan);
    }

    public void assignBasket(String username, Long basketId) throws Exception {
        DeliveryMan deliveryMan = this.deliveryManRepository.getOne(username);
        Basket basket = this.basketRepository.getOne(basketId);

        if (deliveryMan == null || basket == null || !deliveryMan.getRole().equals("DELIVERY_MAN"))
            throw new Exception("Invalid input arguments.");

        deliveryMan.assignBasket(basket);
        basket.setDeliveryMan(deliveryMan);
        basket.setStatus(Constants.DELIVERING);

        this.deliveryManRepository.save(deliveryMan);
    }

    public void deliverBasket(String username, Long basketId) throws Exception {
        DeliveryMan deliveryMan = this.deliveryManRepository.getOne(username);
        Basket basket = this.basketRepository.getOne(basketId);

        if (deliveryMan == null || basket == null || !deliveryMan.getRole().equals("DELIVERY_MAN"))
            throw new Exception("Invalid input arguments.");

        deliveryMan.removeAssigned(basket);
        basket.setStatus(Constants.DELIVERED);

        this.deliveryManRepository.save(deliveryMan);
    }

    public Set<Basket> getAssigned(String username) throws Exception {
        DeliveryMan deliveryMan = this.deliveryManRepository.getOne(username);

        if (deliveryMan == null || !deliveryMan.getRole().equals("DELIVERY_MAN"))
            throw new Exception("Invalid input arguments.");

        return deliveryMan.getAssigned();
    }

    public List<DeliveryMan> getAll() {
        return this.deliveryManRepository.findAll();
    }
}
