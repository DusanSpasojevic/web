package ac.rs.uns.ftn.WebProdavnica.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Customer extends User {
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Article> boughtArticles;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<BasketItem> basket;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Article> favouriteArticles;

    public Customer() {

    }

    public Customer(User user) {
        super(user);
    }

    public void addFavouriteArticle(Article article) {
        favouriteArticles.add(article);
    }

    public Set<Article> getFavouriteArticles() {
        return favouriteArticles;
    }

    public void addArticleToBasket(BasketItem item) {
        basket.add(item);
    }

    public Set<BasketItem> getBasket() { return basket; }

    public void clearBasket() { basket.clear(); }
}
