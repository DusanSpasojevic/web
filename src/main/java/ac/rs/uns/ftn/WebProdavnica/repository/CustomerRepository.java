package ac.rs.uns.ftn.WebProdavnica.repository;

import ac.rs.uns.ftn.WebProdavnica.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, String> {
}
