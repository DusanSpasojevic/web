function loadItemsTable(url) {
    $.getJSON( url, function( data ) {
        var headers = [];
        var items = [];

        headers.push("<th>Name</th>");
        headers.push("<th>Quantity</th>");

        data.forEach(function(item, index) {
            var item = "<tr id=\"" + item.article.articleId + "\">" +
                            "<td>" + item.article.name + "</td>" +
                            "<td>" + item.quantity + "</td>" +
                            "</tr>";
            

            items.push(item);
        })

        if (items.length > 0)
            $("#magicButton").show();
        else
            $("#magicButton").hide();
        
        $("#tableView").html(
            "<table class=\"table\">" +
            "<thead>" +
            headers.join("") +
            "</thead>" +
            "<tbody>" +
            items.join("") +
            "</tbody>" +
            "</table>"
        )
    });
}

// Config articles.
$(document).ready(function() {
    $("magicButton").hide();

    loadItemsTable("http://localhost:8080/api/Customer/BasketArticles", false);

    $("#magicButton").click(function() {
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/api/Basket/Add",
            success: function (data) {
                alert("Thank you for your purchase!");
                // this will be empty moust of the time....
                loadItemsTable("http://localhost:8080/api/Customer/BasketArticles", false);
            },
            error: function(data) {
                alert("Something went wrong.")
            }
        })
    })
})