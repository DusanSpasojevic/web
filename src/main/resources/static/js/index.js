function loadSearchedArticles(url, payload, displayButtons) {
    $.ajax({
                type: "GET",
                url: url,
                data: payload,
                success: function(data) {
                   $.ajax({
                           type: "GET",
                           url: "http://localhost:8080/api/User/Info",
                           contentType: "application/json; charset=utf-8",
                           dataType: "json",
                           data: "",
                           success: function(cred) {
                                   var headers = [];
                                   var items = [];

                                   headers.push("<th>Name</th>");
                                   headers.push("<th>Price</th>");
                                   headers.push("<th>Quantity</th>");
                                   headers.push("<th>Description</th>");
                                   headers.push("<th>Category</th>");

                                   if (displayButtons)
                                       if (cred.role === "CUSTOMER" || cred.role === "ADMIN") {
                                           headers.push("<th></th>");
                                           headers.push("<th></th>");
                                       }

                                   data.forEach(function(article, index) {
                                       var item = "<tr id=\"" + article.articleId + "\">" +
                                                       "<td>" + article.name + "</td>" +
                                                       "<td>" + article.price + "</td>" +
                                                       "<td>" + article.quantity + "</td>" +
                                                       "<td>" + article.description + "</td>" +
                                                       "<td>" + article.category + "</td>";

                                       if (displayButtons && cred.role === "CUSTOMER") {
                                           item += "<td><button class=\"favourite btn btn-light\" articleId=\"" + article.articleId + "\">Favourite" + "</button></td>" +
                                                   "<td><button class=\"buy btn btn-light\" articleId=\"" + article.articleId + "\">Buy" + "</button></td>" +
                                                   "</tr>"
                                       } else if (displayButtons && cred.role === "ADMIN") {
                                           item += "<td><button class=\"update btn btn-light\" articleId=\"" + article.articleId + "\" data-toggle=\"modal\" data-target=\"#articleModal\">Update" + "</button></td>" +
                                                   "<td><button class=\"remove btn btn-light\" articleId=\"" + article.articleId + "\">Remove" + "</button></td>" +
                                                   "</tr>"
                                       } else {
                                           item +=  "</tr>";
                                       }

                                       items.push(item);
                                   })

                                   var articleButton = "<button id=\"addArticle\" class=\"btn btn-primary float-right\"" +
                                       "data-toggle=\"modal\" data-target=\"#articleModal\"" +
                                       ">" +
                                       "Add article" +
                                       "</button>";

                                   $("#tableView").html(
                                       "<table class=\"table\">" +
                                       "<thead>" +
                                       headers.join("") +
                                       "</thead>" +
                                       "<tbody>" +
                                       items.join("") +
                                       "</tbody>" +
                                       "</table>" +
                                       (displayButtons && cred.role === "ADMIN" ? articleButton : "")
                                   )

                                   // Init button function

                                   $(".buy").click(function() {
                                       var id = $(this).attr("articleId");

                                       var val = prompt("Quntity?", "0");

                                       if (val == null)
                                           return ;

                                       var quantity = parseInt(val);

                                       if (!(quantity > 0)) {
                                           alert("Quantity must be positive number!");
                                       } else {
                                           $.ajax({
                                               type: "POST",
                                               url: "http://localhost:8080/api/Customer/AddToBasket/",
                                               data: {
                                                   articleId: id,
                                                   quantity: quantity
                                               },
                                               success: function(data) {
                                                   //alert(data.status);
                                                   //location.reload();
                                                   loadSearchedArticles(url, payload, displayButtons)
                                               },
                                               error: function(data) {
                                                   alert(data.responseJSON.error);
                                               }
                                           })
                                       }
                                   })

                                   $(".favourite").click(function() {
                                       var id = $(this).attr("articleId");
                                       $.ajax({
                                           type: "POST",
                                           url: "http://localhost:8080/api/Customer/AddFavourite/" + id,
                                           success: function(data) {
                                               alert(data.status);
                                           },
                                           error: function(data) {
                                               alert(data.responseJSON.error);
                                           }
                                       })
                                   })

                                   $(".update").click(function() {
                                       var id = $(this).attr("articleId");

                                       var name = $("#" + id + " td:nth-child(1)").text();
                                       var price = parseFloat($("#" + id + " td:nth-child(2)").text());
                                       var quantity = parseInt($("#" + id + " td:nth-child(3)").text());
                                       var description = $("#" + id + " td:nth-child(4)").text();
                                       var category = $("#" + id + " td:nth-child(5)").text();

                                       $("#articleId").val(id);
                                       $("#name").val(name);
                                       $("#description").val(description);
                                       $("#price").val(price);
                                       $("#quantity").val(quantity);
                                       $("#category").val(category);

                                       $("#magicButton").text("Save changes");
                                       $("#magicButton").data("action", "update");
                                   })

                                   $(".remove").click(function() {
                                       var id = $(this).attr("articleId");
                                       $.ajax({
                                           type: "DELETE",
                                           url: "http://localhost:8080/api/Article/" + id,
                                           success: function(data) {
                                               alert(data.status);
                                               loadArticleTable(url, displayButtons);
                                           },
                                           error: function(data) {
                                               alert(data.responseJSON.error);
                                           }
                                       })
                                   })

                                   $("#addArticle").click(function() {

                                       $("#articleId").val("[auto generated]");
                                       $("#name").val("");
                                       $("#description").val("");
                                       $("#price").val("");
                                       $("#quantity").val("");
                                       $("#category").val("");

                                       $("#magicButton").text("Add");
                                       $("#magicButton").data("action", "add");
                                   })
                           }
                       })

                },
                error: function(data) {
                    alert(data);
                }
            })
}

function loadArticleTable(url, displayButtons) {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/api/User/Info",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "",
        success: function(cred) {
            $.getJSON( url, function( data ) {
                var headers = [];
                var items = [];

                headers.push("<th>Name</th>");
                headers.push("<th>Price</th>");
                headers.push("<th>Quantity</th>");
                headers.push("<th>Description</th>");
                headers.push("<th>Category</th>");
                
                if (displayButtons)
                    if (cred.role === "CUSTOMER" || cred.role === "ADMIN") {
                        headers.push("<th></th>");
                        headers.push("<th></th>");
                    }

                data.forEach(function(article, index) {
                    var item = "<tr id=\"" + article.articleId + "\">" +
                                    "<td>" + article.name + "</td>" +
                                    "<td>" + article.price + "</td>" +
                                    "<td>" + article.quantity + "</td>" +
                                    "<td>" + article.description + "</td>" +
                                    "<td>" + article.category + "</td>";
                    
                    if (displayButtons && cred.role === "CUSTOMER") {
                        item += "<td><button class=\"favourite btn btn-light\" articleId=\"" + article.articleId + "\">Favourite" + "</button></td>" +
                                "<td><button class=\"buy btn btn-light\" articleId=\"" + article.articleId + "\">Buy" + "</button></td>" +
                                "</tr>"
                    } else if (displayButtons && cred.role === "ADMIN") {
                        item += "<td><button class=\"update btn btn-light\" articleId=\"" + article.articleId + "\" data-toggle=\"modal\" data-target=\"#articleModal\">Update" + "</button></td>" +
                                "<td><button class=\"remove btn btn-light\" articleId=\"" + article.articleId + "\">Remove" + "</button></td>" +
                                "</tr>"
                    } else {
                        item +=  "</tr>";
                    }

                    items.push(item);
                })

                var articleButton = "<button id=\"addArticle\" class=\"btn btn-primary float-right\"" +
                    "data-toggle=\"modal\" data-target=\"#articleModal\"" +
                    ">" +
                    "Add article" +
                    "</button>";
                
                $("#tableView").html(
                    "<table class=\"table\">" +
                    "<thead>" +
                    headers.join("") +
                    "</thead>" +
                    "<tbody>" +
                    items.join("") +
                    "</tbody>" +
                    "</table>" +
                    (displayButtons && cred.role === "ADMIN" ? articleButton : "")
                )
                
                // Init button function

                $(".buy").click(function() {
                    var id = $(this).attr("articleId");

                    var val = prompt("Quntity?", "0");

                    if (val == null)
                        return ;

                    var quantity = parseInt(val);

                    if (!(quantity > 0)) {
                        alert("Quantity must be positive number!");
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "http://localhost:8080/api/Customer/AddToBasket/",
                            data: {
                                articleId: id,
                                quantity: quantity
                            },
                            success: function(data) {
                                //alert(data.status);
                                //location.reload();
                                loadArticleTable(url, displayButtons);
                            },
                            error: function(data) {
                                alert(data.responseJSON.error);
                            }
                        })
                    }
                })

                $(".favourite").click(function() {
                    var id = $(this).attr("articleId");
                    $.ajax({
                        type: "POST",
                        url: "http://localhost:8080/api/Customer/AddFavourite/" + id,
                        success: function(data) {
                            alert(data.status);
                        },
                        error: function(data) {
                            alert(data.responseJSON.error);
                        }
                    })
                })

                $(".update").click(function() {
                    var id = $(this).attr("articleId");

                    var name = $("#" + id + " td:nth-child(1)").text();
                    var price = parseFloat($("#" + id + " td:nth-child(2)").text());
                    var quantity = parseInt($("#" + id + " td:nth-child(3)").text());
                    var description = $("#" + id + " td:nth-child(4)").text();
                    var category = $("#" + id + " td:nth-child(5)").text();

                    $("#articleId").val(id);
                    $("#name").val(name);
                    $("#description").val(description);
                    $("#price").val(price);
                    $("#quantity").val(quantity);
                    $("#category").val(category);

                    $("#magicButton").text("Save changes");
                    $("#magicButton").data("action", "update");
                })

                $(".remove").click(function() {
                    var id = $(this).attr("articleId");
                    $.ajax({
                        type: "DELETE",
                        url: "http://localhost:8080/api/Article/" + id,
                        success: function(data) {
                            alert(data.status);
                            loadArticleTable(url, displayButtons);
                        },
                        error: function(data) {
                            alert(data.responseJSON.error);
                        }
                    })
                })

                $("#addArticle").click(function() {

                    $("#articleId").val("[auto generated]");
                    $("#name").val("");
                    $("#description").val("");
                    $("#price").val("");
                    $("#quantity").val("");
                    $("#category").val("");
                    
                    $("#magicButton").text("Add");
                    $("#magicButton").data("action", "add");
                })
            });
        }
    })
}


function loadCategories(url) {
    $.getJSON( url, function( data ) {
        var items = [];

        data.forEach(function(item, index) {
            item = "<option value=\"" + item + "\">" + item + "</option>";
            items.push(item);
        });

        $("#searchCategory").html(
            items.join("")
        )
    });
}

// Config articles.
$(document).ready(function() {
    loadArticleTable("http://localhost:8080/api/Article/all", true);
    loadCategories("http://localhost:8080/api/Article/Categories");

    $("#magicButton").click(function() {
        var price = parseFloat($("#price").val());
        var quantity = parseInt($("#quantity").val());

        var data;

        if ($(this).data("action") === "update")
            data = {
                articleId: $("#articleId").val(),
                name: $("#name").val(),
                description: $("#description").val(),
                price: price,
                quantity: quantity,
                category: $("#category").val()
            }
        else if ($(this).data("action") === "add")
            data = {
                name: $("#name").val(),
                description: $("#description").val(),
                price: price,
                quantity: quantity,
                category: $("#category").val()
            }
        else
            return;

        var articleJSON = JSON.stringify(data)

        if (price == null || quantity == null) {
            console.log("Something is null...");
            return;
        }
        
        if (price <= 0.0 || quantity <= 0) {
            console.log("Somethin is less (or equal) then zero...");
            return;
        }
        
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/api/Article/Add",
            dataType: "json",
            contentType: "application/json",
            data: articleJSON,
            success: function (data) {
                $('.modal').modal('hide');
                loadArticleTable("http://localhost:8080/api/Article/all", true);
            },
            error: function(data) {
            }
        })
    });

    $("#search").submit(function( event ) {
        event.preventDefault();

        var searchObject = {};

        var name = $("#searchName").val();
        var price = $("#searchPrice").val();
        var desc = $("#searchDescription").val();

        if (name !== "")
            searchObject.name = name;
        if (price !== "")
            searchObject.price = parseFloat(price);
        if (desc !== "")
            searchObject.description = desc;

        if (name === "" && price === "" && desc === "") {
            loadArticleTable("http://localhost:8080/api/Article/all", true);
            return;
        }

        loadSearchedArticles("http://localhost:8080/api/Article/Search", searchObject, true)
      });

    $("#displayCategory").submit(function(event) {
        event.preventDefault();
        loadSearchedArticles("http://localhost:8080/api/Article/category/" + $("#searchCategory").val(), "", true);
    })
})