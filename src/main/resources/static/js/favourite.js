function loadArticleTable(url, displayButtons) {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/api/User/Info",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "",
        success: function(cred) {
            $.getJSON( url, function( data ) {
                var headers = [];
                var items = [];

                headers.push("<th>Name</th>");
                headers.push("<th>Price</th>");
                headers.push("<th>Quantity</th>");
                headers.push("<th>Description</th>");
                headers.push("<th>Category</th>");
                
                if (displayButtons)
                    if (cred.role === "CUSTOMER" || cred.role === "ADMIN") {
                        headers.push("<th></th>");
                        headers.push("<th></th>");
                    }

                data.forEach(function(article, index) {
                    var item = "<tr id=\"" + article.articleId + "\">" +
                                    "<td>" + article.name + "</td>" +
                                    "<td>" + article.price + "</td>" +
                                    "<td>" + article.quantity + "</td>" +
                                    "<td>" + article.description + "</td>" +
                                    "<td>" + article.category + "</td>";
                    
                    if (displayButtons && cred.role === "CUSTOMER") {
                        item += "<td><button class=\"favourite\" articleId=\"" + article.articleId + "\">Favourite" + "</button></td>" +
                                "<td><button class=\"buy\" articleId=\"" + article.articleId + "\">Buy" + "</button></td>" +
                                "</tr>"
                    } else if (displayButtons && cred.role === "ADMIN") {
                        item += "<td><button class=\"update\" articleId=\"" + article.articleId + "\">Update" + "</button></td>" +
                                "<td><button class=\"remove\" articleId=\"" + article.articleId + "\">Remove" + "</button></td>" +
                                "</tr>"
                    } else {
                        item +=  "</tr>";
                    }

                    items.push(item);
                })
                
                $("#tableView").html(
                    "<table class=\"table\">" +
                    "<thead>" +
                    headers.join("") +
                    "</thead>" +
                    "<tbody>" +
                    items.join("") +
                    "</tbody>" +
                    "</table>"
                )
                
                // Init button function

                $(".buy").click(function() {
                    alert($(this).attr("articleId"));
                })

                $(".favourite").click(function() {
                    var id = $(this).attr("articleId");
                    $.ajax({
                        type: "POST",
                        url: "http://localhost:8080/api/Customer/AddFavourite/" + id,
                        success: function(data) {
                            alert(data.status);
                        },
                        error: function(data) {
                            alert(data.responseJSON.error);
                        }
                    })
                })
            });
        }
    })
}

// Config articles.
$(document).ready(function() {
    loadArticleTable("http://localhost:8080/api/Customer/Favourite", false);
})