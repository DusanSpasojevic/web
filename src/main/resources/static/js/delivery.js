function loadDelivering(url) {
    $.getJSON(url, function( basket ) {
        if (basket.id == undefined || basket.time == undefined)
        {
            return;
        }

        var headers = [];
        var items = [];

        headers.push("<th>ID</th>");
        headers.push("<th>Time</th>");
        headers.push("<th></th>");


        var item = "<tr id=\"" + basket.id + "\">" +
                            "<td>" + basket.id + "</td>" +
                            "<td>" + basket.time + "</td>" +
                            "<td><button class=\"deliver btn btn-light\" data-basketid=\"" + basket.id + "\">Deliver" + "</button></td>";
        items.push(item);

        $("#tableDeliveringView").html(
            "<table class=\"table\">" +
            "<thead>" +
            headers.join("") +
            "</thead>" +
            "<tbody>" +
            items.join("") +
            "</tbody>" +
            "</table>"
        );


        $(".deliver").click(function() {
            var id = $(this).data("basketid");
            //$("#" + id).hide();
            
            $.ajax({
                type: "POST",
                url: "http://localhost:8080/api/Basket/Deliver/" + id,
                success: function(data) {
                    $("#" + id).remove();
                    alert("Delivered.");
                },
                error: function(data) {
                    alert(data.responseJSON.error);
                }
            })
        })
    });
}

function loadOpenDeliveries(url) {
    $.getJSON(url, function( data ) {
        var headers = [];
        var items = [];

        headers.push("<th>ID</th>");
        headers.push("<th>Time</th>");
        headers.push("<th></th>");

        data.forEach(function(basket, index) {
            var item = "<tr id=\"" + basket.id + "\">" +
                            "<td>" + basket.id + "</td>" +
                            "<td>" + basket.time + "</td>" +
                            "<td><button class=\"pick btn btn-light\" data-basketid=\"" + basket.id + "\">Pick" + "</button></td>";
            items.push(item);
        });

        $("#tableOpenDeliveriesView").html(
            "<table class=\"table\">" +
            "<thead>" +
            headers.join("") +
            "</thead>" +
            "<tbody>" +
            items.join("") +
            "</tbody>" +
            "</table>"
        );

        $(".pick").click(function() {
            var id = $(this).data("basketid");
            //$("#" + id).hide();
            
            $.ajax({
                type: "POST",
                url: "http://localhost:8080/api/Basket/Delivering/" + id,
                success: function(data) {
                    $("#" + id).remove();
                    loadDelivering("http://localhost:8080/api/Basket/Delivering");
                },
                error: function(data) {
                    alert(data.responseJSON.error);
                }
            })
        })
    });
}

$(document).ready(function() {
    loadOpenDeliveries("http://localhost:8080/api/Basket/Bought");
    loadDelivering("http://localhost:8080/api/Basket/Delivering");
})